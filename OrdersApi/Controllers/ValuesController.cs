﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OrdersApi.Controllers
{
    [Route("api/orders")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Order>> Get()
        {
            return new Order[] {
                new Order("101", 200),
                new Order("102", 400)
             };
        }
    }

    public class Order
    {
        public string Id { get; set; }
        public decimal Ammount { get; set; }

        public Order(string id, decimal ammount)
        {
            Id = id;
            Ammount = ammount;
        }
    }
}
